.. toctree::
   :maxdepth: 1
   :caption: Quickstart guide 
   :hidden:

   quickstart

.. toctree::
   :maxdepth: 1
   :caption: Merging Sequencing Runs
   :hidden:

   mergingruns

.. toctree::
   :maxdepth: 1
   :caption: More tips
   :hidden:

   moretips

.. toctree::
   :maxdepth: 1
   :caption: List of Tourmaline Features
   :hidden:

   listoffeatures

.. toctree::
   :maxdepth: 1
   :caption: List of software/database versions
   :hidden:

   listofsoftwareversions

Tourmaline
==========

Tourmaline is an amplicon sequence processing workflow for Illumina
sequence data that uses `QIIME 2 <https://qiime2.org>`__ and the
software packages it wraps. Tourmaline manages commands, inputs, and
outputs using the
`Snakemake <https://snakemake.readthedocs.io/en/stable/>`__ workflow
management system.

*This version (Tourmaline-MMB-UMCU) is an adaptation of* 
`Tourmaline <https://github.com/aomlomics/tourmaline>`__ *developed specifically 
for the Medical Microbiology Department at the UMC Utrecht. The Tourmaline-MMB-UMCU has been adapted for the use of the 16S rRNA dual-index primers as developed by Fadrosh et al. (Microbiome, 2014) and uses not yet demultiplexed fastq files as input.*

Getting Started
+++++++++++++++

The :ref:`Quick Start Guide <quickstart>` is a great place to start, containing short descriptions 
of the different steps needed to run the pipeline along with some example commands.
In case you have multiple sequencing runs that you would like to merge, jump to :ref:`Merging Sequencing Runs <mergingruns>`.

Citing Tourmaline
+++++++++++++++++

The Tourmaline paper is published in GigaScience:

Thompson, L. R., Anderson, S. R., Den Uyl, P. A., Patin, N. V., Lim, S. J., Sanderson, G. & Goodwin, K. D. Tourmaline: A containerized workflow for rapid and iterable amplicon sequence analysis using QIIME 2 and Snakemake. GigaScience, Volume 11, 2022, giac066, https://doi.org/10.1093/gigascience/giac066
