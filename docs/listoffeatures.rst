.. _listoffeatures:

Tourmaline features
+++++++++++++++++++

Tourmaline has several features that enhance usability and
interoperability:

-  **Portability.** Native support for Linux and macOS in addition to
   Docker containers.
-  **QIIME 2.** The core commands of Tourmaline, including the
   `DADA2 <https://benjjneb.github.io/dada2/index.html>`__ and
   `Deblur <https://github.com/biocore/deblur>`__ packages, are all
   commands of QIIME 2, one of the most popular amplicon sequence
   analysis software tools available. You can print all of the QIIME 2
   and other shell commands of your workflow before or while running the
   workflow.
-  **Snakemake.** Managing the workflow with Snakemake provides several
   benefits:

   -  **Configuration file** contains all parameters in one file, so you
      can see what your workflow is doing and make changes for a
      subsequent run.
   -  **Directory structure** is the same for every Tourmaline run, so
      you always know where your outputs are.
   -  **On-demand commands** mean that only the commands required for
      output files not yet generated are run, saving time and
      computation when re-running part of a workflow.

-  **Parameter optimization.** The configuration file and standard
   directory structure make it simple to test and compare different
   parameter sets to optimize your workflow. Included code helps choose
   read truncation parameters and identify outliers in representative
   sequences (ASVs).
-  **Visualizations and reports.** Every Tourmaline run produces an HTML
   report containing a summary of your metadata and outputs, with links
   to web-viewable QIIME 2 visualization files.
-  **Downstream analysis.** Analyze the output of single or multiple
   Tourmaline runs programmatically, with qiime2R in R or the QIIME 2
   Artifact API in Python, using the provided R and Python notebooks or
   your own code.
