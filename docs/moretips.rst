.. _moretips:

Output
++++++

After completion of a Tourmaline run, a number of different output folders and files will appear in the directory. In general, the most important files for further downstream analyses are located in the ``02-output-dada2-pe-unfiltered`` folder. This folder should contain, a.o.: 

 *  ASV tables in tsv-format (``02-output-dada2-pe-unfiltered/00-table-repseqs/table*.tsv``) 
 *  Taxonomy table in tsv-format (``02-output-dada2-pe-unfiltered/01-taxonomy/taxonomy.tsv``)
 *  QIIME2 visualization files (QZV-files). 
**TIP:** A good way to quickly evaluate the results, would be to first have a look at the file ``02-output-dada2-pe-unfiltered/00-table-repseqs/dada2_stats.qzv``. This file contains an overview on the different filtering steps that were performed. A low percentage of input that passed filtering could be an indication that the truncation settings were not optimal. In this case it is advisable to check the fastqc output files again. See below for instuctions on how to view these type of files. 

Viewing QZV-files
++++++++++++++++++

To view .qzv output (QIIME 2 visualization) files, drag and drop these in https://view.qiime2.org. Empress trees (i.e. rooted_tree.qzv) may take more than 10 minutes to load. For a lot of visualization files in table format it is also possible to download these as tsv-files by clicking "Download metadata TSV file" in the top left corner. 

Run duration
++++++++++++

The whole workflow with test data should take ~30 minutes to complete. A normal dataset may take several hours to complete depending on the size and amount of samples.

Re-running Tourmaline
+++++++++++++++++++++

If you want to re-run and not save previous output, run the following
command (while inside your project folder) to remove ALL previously generated output:

::

   srun --pty bash
   ./scripts/remove_output.sh

Then re-run Tourmaline as before. Example: 
::

   ./sbatch_run_tourmaline taxonomy
