#!/bin/bash

rm -r 02-output-dada2-pe-unfiltered/
rm -r 03-reports/
rm -r 00-data/*.done
rm -r 00-data/manifest_*
rm -r 00-data/fastq/*
rm 01-imported/fastq_*
rm 01-imported/*.done
rm 01-imported/metadata_*
rm -r logs/

singularity exec -H $PWD -B $PWD:/data -B $TMPDIR:$TMPDIR -B /hpc/tmp/:/home/qiime2/ --pwd data /hpc/local/CentOS7/dla_mm/tools/qiime2/qiime2_tourmaline_container/tourmaline-qiime2-2022.2_latest.sif snakemake --unlock
